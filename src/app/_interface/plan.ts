import { IZaznam } from './zaznam';
import { IRidicZaznam } from './ridic-zaznam';
export class IPlan {
  constructor(
    public cPlanu: number = null,
    public nazev = '',
    public datum: Date = null,
    public vozidlo: string = null,
    public ridic1: number = null,
    public ridic2: number = null,
    public popis: string = null,
    public zmeneno: Date = null,
    public zaznamy: Array<IZaznam> = null,
    public ridicZaznamy: Array<IRidicZaznam> = null
  ) {}
}
