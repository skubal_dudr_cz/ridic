import { EnumTypServisu } from './typ-servisu.enum';

export class IRidicServis {
  constructor(
    public id: number = 0,
    public cZaznamu: number = null,
    public typ: EnumTypServisu = null,
    public mnozstvi: number = null,
    public operace: string = '',
    public poznamka: string = ''
  ) {}
}
