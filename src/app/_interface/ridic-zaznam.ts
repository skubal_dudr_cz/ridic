import { IRidicServis } from './ridic-servis';
import { IKarta } from './karta';

export class IRidicZaznam {
  constructor(
    public id: number = 0,
    public cZaznamu: number = null,
    public cPlanu: number = null,
    public cKarty: number = null,
    public cUzivatele: number = null,
    public stavZavozu: string = null,
    public servisTerminStandard = true,
    public mnozstviNovych: number = null,
    public poznamka: string = null,
    public ridicServis: Array<IRidicServis> = [],
    public karta: IKarta = null
  ) {}
}
