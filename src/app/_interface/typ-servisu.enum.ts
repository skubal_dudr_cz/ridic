export enum EnumTypServisu {
  Pas = 'pás',
  Kotouc = 'kotouč',
  Katr = 'katr',
  Jine = 'jiné'
}
