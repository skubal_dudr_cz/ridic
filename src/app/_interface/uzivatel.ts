export class IUzivatel {
  constructor(
    public cUzivatele: number = null,
    public jmeno = '',
    public login = '',
    public pozice = '',
    public ridic = false,
    public spravaPlanu = false
  ) {}
}
