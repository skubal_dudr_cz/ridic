import { IRidicZaznam } from './ridic-zaznam';
export class IZaznam {
  constructor(
    public cZaznamu: number = null,
    public cPlanu: number = null,
    public cKarty: number = 0,
    public text = '',
    public termin: Date = null,
    public staloSeKdy: Date = null,
    public poradiVPlanu: number = null,
    public karta: any = null, // zakaznik
    public ridicZaznam: IRidicZaznam = null,
    public changed: Boolean = false
  ) {}
}
