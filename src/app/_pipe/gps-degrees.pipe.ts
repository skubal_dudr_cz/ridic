import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gpsDegrees'
})
export class GpsDegreesPipe implements PipeTransform {
  transform(gpsCoords: string, args?: any): string {
    const gps = gpsCoords.split(',');
    return this.formatGps(gps[0]) + ', ' + this.formatGps(gps[1]);
  }

  formatGps(gpsCoord) {
    // D° M' S" = D° (0.ddd * 60)' (0.ddd * 3600)"
    const stupne = Math.floor(gpsCoord);
    const minuty = Math.floor((gpsCoord - stupne) * 60);
    const vteriny = Math.round((gpsCoord - (stupne + minuty / 60)) * 3600 * 100) / 100;
    return stupne + '°' + minuty + '\'' + vteriny + '"';
  }
}
