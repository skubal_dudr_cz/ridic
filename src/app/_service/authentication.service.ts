import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { startWith, map, catchError } from 'rxjs/operators';
import { Md5 } from 'ts-md5/dist/md5';

import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import { IUzivatel } from '../_interface/uzivatel';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService  extends BaseService {

  uzivatel: IUzivatel;
  private authUrl = environment.dotoUrl + 'Uzivatels';
  private authSource = new BehaviorSubject('');
  currentUser = this.authSource.asObservable().pipe(startWith(localStorage.getItem('currentUser')));

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  login(username: string, password: string): Observable<IUzivatel> {
    const filter = {
      fields: Object.keys(new IUzivatel()),
      where: {
        login: username,
        heslo: Md5.hashStr(password)
      }
    };

    const url = this.authUrl + '/findOne?filter=' + JSON.stringify(filter);
    return this.http.get<IUzivatel>(url)
      .pipe(
        map(user => {
          // login successful if there's a jwt token in the response
          // if (user && user.token) {
          if (user) {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            const loggedUser = user;
            localStorage.setItem('currentUser', JSON.stringify(loggedUser));
            this.authSource.next(JSON.stringify(loggedUser));
            return loggedUser;
          }
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.authSource.next(null);
  }
}
