import { Observable, of, throwError } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

export class BaseService {
  protected httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor() {}

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  protected handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(`${operation} failed: ${error.message}`); // log to console instead

      throwError(error);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
