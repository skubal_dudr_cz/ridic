import { IKarta } from './../_interface/karta';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class KartaService extends BaseService {
  private url = environment.dotoUrl + 'Karta';

  constructor(private http: HttpClient) {
    super();
  }

  getList(): Observable<Array<IKarta>> {
    const filter = {
      fields: ['cKarty', 'nazev'],
      where: {
        maObjednavky: true
      },
      order: 'nazev ASC'
    };

    const url = this.url + '?filter=' + JSON.stringify(filter);
    return this.http.get<IKarta[]>(url);
  }
}
