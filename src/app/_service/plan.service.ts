import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import { IPlan } from '../_interface/plan';
import { IUzivatel } from '../_interface/uzivatel';
import { IRidicServis } from './../_interface/ridic-servis';
import { IRidicZaznam } from './../_interface/ridic-zaznam';
import { IZaznam } from './../_interface/zaznam';

@Injectable({
  providedIn: 'root'
})
export class PlanService extends BaseService {
  private url = environment.dotoUrl + 'Plans';
  private source = new BehaviorSubject('');
  currentUser = this.source.asObservable();

  constructor(private http: HttpClient) {
    super();
  }

  getListForUser(user: IUzivatel): Observable<Array<IPlan>> {
    const filter = {
      include: [
        {
          relation: 'prvniRidic',
          scope: {
            fields: ['jmeno']
          }
        },
        {
          relation: 'druhyRidic',
          scope: {
            fields: ['jmeno']
          }
        }
      ],
      fields: Object.keys(new IPlan()),
      where: {
        or: [
          {
            ridic1: user.cUzivatele
          },
          {
            ridic2: user.cUzivatele
          }
        ]
      },
      order: 'datum DESC',
      limit: 10
    };

    const url = this.url + '?filter=' + JSON.stringify(filter);
    return this.http.get<IPlan[]>(url);
  }

  getForUser(cPlanu: number, user: IUzivatel): Observable<IPlan> {
    const filter = {
      include: [
        {
          relation: 'prvniRidic',
          scope: {
            fields: ['jmeno']
          }
        },
        {
          relation: 'druhyRidic',
          scope: {
            fields: ['jmeno']
          }
        },
        {
          relation: 'zaznamy',
          scope: {
            include: [
              {
                relation: 'karta',
                scope: {
                  fields: [
                    'nazev',
                    'kontaktniOsoba',
                    'telefon',
                    'telefonPoznamka',
                    'mobil',
                    'mobilPoznamka',
                    'dUlice',
                    'dMesto',
                    'dPsc',
                    'dStat',
                    'gps',
                    'presnostGps',
                    'fakturace',
                    'poznamkaFakturace'
                  ]
                }
              },
              {
                relation: 'ridicZaznam',
                scope: {
                  include: [
                    {
                      relation: 'ridicServis',
                      scope: {
                        fields: Object.keys(new IRidicServis())
                      }
                    }
                  ],
                  fields: Object.keys(new IRidicZaznam())
                }
              }
            ],
            fields: Object.keys(new IZaznam()),
            order: 'poradiVPlanu'
          }
        },
        {
          relation: 'ridicZaznamy',
          scope: {
            include: [
              {
                relation: 'karta',
                scope: {
                  fields: [
                    'nazev',
                    'kontaktniOsoba',
                    'telefon',
                    'telefonPoznamka',
                    'mobil',
                    'mobilPoznamka',
                    'dUlice',
                    'dMesto',
                    'dPsc',
                    'dStat',
                    'gps',
                    'presnostGps'
                  ]
                }
              },
              {
                relation: 'ridicServis',
                scope: {
                  fields: Object.keys(new IRidicServis())
                }
              }
            ],
            fields: Object.keys(new IRidicZaznam()),
            where: {
              cZaznamu: null
            }
          }
        }
      ],
      fields: Object.keys(new IPlan()),
      where: {
        or: [
          {
            ridic1: user.cUzivatele
          },
          {
            ridic2: user.cUzivatele
          }
        ]
      }
    };

    const url = this.url + '/' + cPlanu + '?filter=' + JSON.stringify(filter);
    return this.http.get<IPlan>(url);
  }
}
