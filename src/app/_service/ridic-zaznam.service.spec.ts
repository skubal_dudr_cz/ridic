import { TestBed } from '@angular/core/testing';

import { RidicZaznamService } from './ridic-zaznam.service';

describe('RidicZaznamService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RidicZaznamService = TestBed.get(RidicZaznamService);
    expect(service).toBeTruthy();
  });
});
