import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import { IRidicZaznam } from './../_interface/ridic-zaznam';

@Injectable({
  providedIn: 'root'
})
export class RidicZaznamService extends BaseService {
    private url = environment.dotoUrl + 'RidicZaznams';
    private source = new BehaviorSubject('');
    currentUser = this.source.asObservable();

    constructor(private http: HttpClient) {
      super();
    }

  upsertRidicZaznam(ridicZaznam: IRidicZaznam): Observable<IRidicZaznam> {
    const url = this.url + '/replaceOrCreateWithRelations?purge=true';
    return this.http.post<IRidicZaznam>(url, ridicZaznam, this.httpOptions)
    .pipe(
      catchError(this.handleError<IRidicZaznam>(`upsertRidicZaznam id=${ridicZaznam.id}`))
    );
  }

}
