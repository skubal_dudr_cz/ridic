import { Component, OnInit } from '@angular/core';
import { VersionCheckNg6LibService } from 'version-check-ng6-lib';

import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  versionCheckFrequency = 1000 * 60 * 30; // every 30 minutes

  constructor(
    private versionCheckService: VersionCheckNg6LibService
  ) {}

  ngOnInit(): void {
    if (environment.production) {
      this.versionCheckService.initVersionCheck(
        '/version.json',
        this.versionCheckFrequency,
        this.newVersionDetected
      );
    }
  }

  private newVersionDetected(hash: string): void {
    console.log('New version has been detected:', hash);
    if (confirm('Nová verze je k dispozici, načíst znovu?')) {
      location.reload();
    }
  }

}
