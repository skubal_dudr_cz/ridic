import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module
import { CdkTableModule } from '@angular/cdk/table';

import {
  MatToolbarModule,
  MatButtonModule,
  MatMenuModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatCardModule,
  MatDividerModule,
  MatSnackBarModule,
  MatCheckboxModule,
  MatInputModule,
  MatRadioModule,
  MatDatepickerModule,
  MatSelectModule,
  MatStepperModule,
  MatAutocompleteModule,
} from '@angular/material';

import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PlanComponent } from './pages/plan/plan.component';
import { LoginComponent } from './pages/login/login.component';
import { EntryComponent } from './ui/entry/entry.component';

import { AuthenticationService } from './_service/authentication.service';
import { AuthGuard } from './_guard/auth.guard';
import { GpsDegreesPipe } from './_pipe/gps-degrees.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlanComponent,
    LoginComponent,
    EntryComponent,
    GpsDegreesPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CdkTableModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatCardModule,
    MatDividerModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatDatepickerModule,
    MatSelectModule,
    MatStepperModule,
    MatAutocompleteModule,
    AppRoutingModule
  ],
  exports: [
    AppComponent,
    LoginComponent,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthenticationService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
