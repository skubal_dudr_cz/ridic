import { Component, OnInit, OnDestroy } from '@angular/core';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { IUzivatel } from 'src/app/_interface/uzivatel';
import { IPlan } from 'src/app/_interface/plan';
import { PlanService } from 'src/app/_service/plan.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  public isProduction = environment.production;
  user: IUzivatel;
  plany$: Observable<Array<IPlan>>;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));

  constructor(
    private authenticationService: AuthenticationService,
    private breakpointObserver: BreakpointObserver,
    private planService: PlanService
  ) {}

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(userJSON => {
      if (userJSON) {
        this.user = JSON.parse(userJSON);
      }
    });
    this.plany$ = this.planService.getListForUser(this.user);
  }

  ngOnDestroy() {
    // this.authenticationService.currentUser.unsubscribe();
  }
}
