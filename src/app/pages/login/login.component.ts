import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  isProduction = environment.production;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private breakpointObserver: BreakpointObserver,
    public snackBar: MatSnackBar
  ) {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService
      .login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        user => {
          if (user.cUzivatele) {
            this.router.navigate([this.returnUrl]);
          } else {
            this.loading = false;
            const message =
              'Nepodařilo se přihlásit - zadali jste špatné přihlašovací jméno nebo heslo.';
            this.snackBar.open(message, 'Zavřít', {
              duration: 3000
            });
          }
        },
        error => {
          console.error(error);
          if ( error.status === 404 ) {
            this.snackBar.open('Zadány nesprávné přihlašovací údaje.', 'Zavřít', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Databázový server neodpovídá. Zkuste to prosím později.', 'Zavřít', {
              duration: 3000
            });
          }
          this.loading = false;
        }
      );
  }

  fullScreenOn() {
    const elem = document.documentElement;
    const methodToBeInvoked =
      elem['requestFullscreen'] ||
      elem['webkitRequestFullScreen'] ||
      elem['mozRequestFullScreen'] ||
      elem['msRequestFullscreen'];
    if (methodToBeInvoked) {
      methodToBeInvoked.call(elem);
    }
  }

  fullScreenOff() {
    const elem = document.documentElement;
    const methodToBeInvoked =
      elem['exitFullscreen'] ||
      elem['webkitExitFullscreen'] ||
      elem['mozCancelFullScreen'] ||
      elem['msExitFullscreen'];
    if (methodToBeInvoked) {
      methodToBeInvoked.call(elem);
    }
  }

}
