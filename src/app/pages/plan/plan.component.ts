import { Component, OnInit, OnDestroy } from '@angular/core';
import { _sanitizeHtml } from '@angular/core/src/sanitization/html_sanitizer';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { IUzivatel } from 'src/app/_interface/uzivatel';
import { IPlan } from 'src/app/_interface/plan';
import { IKarta } from 'src/app/_interface/karta';
import { IZaznam } from './../../_interface/zaznam';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { PlanService } from 'src/app/_service/plan.service';
import { KartaService } from './../../_service/karta.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit, OnDestroy {
  public isProduction = environment.production;
  loggedUser: IUzivatel;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));
  plan: IPlan;
  entry: IZaznam;
  selectedEntryIndex: number;
  clients: Array<IKarta>;
  routeCoords: string;
  filteredClients: Observable<Array<IKarta>>;
  clientControl = new FormControl();

  constructor(
    private authenticationService: AuthenticationService,
    private planService: PlanService,
    private kartaService: KartaService,
    private route: ActivatedRoute,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(userJSON => {
      if (userJSON) {
        this.loggedUser = JSON.parse(userJSON);
      }
    });

    this.planService
      .getForUser(+this.route.snapshot.paramMap.get('cPlanu'), this.loggedUser)
      .subscribe(plan => {
        this.plan = plan;
        this.plan.ridicZaznamy.forEach(ridicZaznam => {
          const _zaznam = new IZaznam(
            null,
            plan.cPlanu,
            ridicZaznam.cKarty,
            null,
            null,
            null,
            null,
            ridicZaznam.karta,
            ridicZaznam,
            false
          );
          this.plan.zaznamy.push(_zaznam);
        });
        this.selectEntry(0);
      });

    this.kartaService.getList().subscribe(_clients => {
      this.clients = _clients;
      this.filteredClients = this.clientControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filterClients(value))
      );
    });
  }

  private _filterClients(value: string | IKarta): Array<IKarta> {
    if (typeof value === 'string') {
      const filterValue = value.toLowerCase();
      return this.clients.filter(_client =>
        _client.nazev.toLowerCase().includes(filterValue)
      );
    } else {
      return this.clients;
    }
  }

  ngOnDestroy() {
    // this.authenticationService.currentUser.unsubscribe();
  }

  selectEntry(index: number): void {
    if (index !== null) {
      this.entry = this.plan.zaznamy[index];
      this.routeCoords = '';
      this.plan.zaznamy.forEach(zaznam => {
        if ( zaznam.karta.gps && zaznam.poradiVPlanu != null && zaznam.poradiVPlanu >= this.entry.poradiVPlanu) {
          this.routeCoords = this.routeCoords + zaznam.karta.gps + '/';
        }
      });
    } else {
      this.entry = null;
    }
    this.selectedEntryIndex = index;
  }

  displayClientFn(client?: IKarta): string | undefined {
    return client ? client.nazev : undefined;
  }

  createEntry(_client: IKarta) {
    const newEntry: IZaznam = new IZaznam(
      null,
      +this.route.snapshot.paramMap.get('cPlanu'),
      _client.cKarty,
      '',
      null,
      null,
      null,
      _client,
      null,
      false
    );
    this.plan.zaznamy.push(newEntry);
    this.selectEntry(this.plan.zaznamy.length - 1);
  }
}
