import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter
} from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { environment } from './../../../environments/environment';
import { IRidicZaznam } from './../../_interface/ridic-zaznam';
import { IUzivatel } from './../../_interface/uzivatel';
import { IZaznam } from './../../_interface/zaznam';
import { IRidicServis } from './../../_interface/ridic-servis';
import { EnumTypServisu } from './../../_interface/typ-servisu.enum';
import { RidicZaznamService } from './../../_service/ridic-zaznam.service';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent implements OnInit, OnChanges {
  @Input()
  entry: IZaznam;
  @Input()
  user: IUzivatel;
  @Input()
  routeCoords: string;
  @Output()
  saveEvent = new EventEmitter<boolean>();
  config: any;
  editedServisId: number;
  locked = false;

  constructor(
    private ridicZaznamService: RidicZaznamService,
    private snackBar: MatSnackBar,
    private ref: ChangeDetectorRef
  ) {
    this.config = environment.ridic;
  }

  ngOnInit() {
    if (!this.entry.ridicZaznam) {
      this.entry.ridicZaznam = this.newRidicZaznam(this.entry.cZaznamu);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('entry' in changes && !changes.entry.firstChange) {
      if (!changes.entry.currentValue.ridicZaznam) {
        this.entry.ridicZaznam = this.newRidicZaznam(this.entry.cZaznamu);
      }
    }
  }

  newRidicZaznam(cZaznamu: number) {
    this.entry.changed = false;
    return new IRidicZaznam(
      0,
      cZaznamu,
      this.entry.cPlanu,
      this.entry.cKarty,
      this.user.cUzivatele
    );
  }

  newRidicServis() {
    this.entry.changed = false;
    return new IRidicZaznam(
      0,
      this.entry.cPlanu,
      this.entry.cKarty,
      this.user.cUzivatele
    );
  }

  getObjectKeys(object: Object): Array<string> {
    return Object.keys(object);
  }

  isOperationChecked(operation: string, operations: string) {
    return operations.split(',').includes(operation);
  }

  operationChange($event, operation: string, operations: string, i: number) {
    let arr = operations ? operations.split(',') : [];
    if ($event.checked) {
      if (!arr.includes(operation)) {
        arr.push(operation);
      }
    } else {
      if (arr.includes(operation)) {
        arr = arr.filter(op => op !== operation);
      }
    }
    this.entry.ridicZaznam.ridicServis[i].operace = arr.join(',');
    this.onChange();
  }

  parseOperations(operations: string) {
    return operations.split(',');
  }

  servisEdit(id): void {
    this.editedServisId = id;
  }

  servisDelete(id): void {
    if (
      confirm(
        'Opravdu chcete smazat položku ' +
          this.extractServisItem(
            this.entry.ridicZaznam.ridicServis[id]
          ).replace(/<(?:.|\n)*?>/gm, '') +
          '?'
      )
    ) {
      this.entry.ridicZaznam.ridicServis.splice(id, 1);
      this.onChange();
    }
  }

  onChange() {
    this.entry.changed = true;
  }

  extractServis(ridicZaznam: IRidicZaznam): String {
    let result = '';
    if (ridicZaznam && ridicZaznam.ridicServis) {
      if (!ridicZaznam.servisTerminStandard) {
        result += '<b>termín v poznámce</b> ';
      }
      const result_items = [];
      for (const servis of ridicZaznam.ridicServis) {
        result_items.push(this.extractServisItem(servis));
      }
      result += result_items.join(', ');
    }
    return result;
  }

  extractServisItem(ridicServis: IRidicServis): String {
    let result = '';
    result += (ridicServis['mnozstvi'] ? ridicServis['mnozstvi'] : '?') + '× ' + ridicServis.typ;
    if (ridicServis.operace) {
      result += ' <small>(' + ridicServis.operace + ')</small>';
    }
    return result;
  }

  servisPridat(typ: EnumTypServisu): void {
    const newItem = new IRidicServis(0, this.entry.ridicZaznam.id, typ);

    this.entry.ridicZaznam.ridicServis.push(newItem);
    this.editedServisId = this.entry.ridicZaznam.ridicServis.length - 1;
    this.onChange();
  }

  upsertRidicZaznam(emitSave: boolean) {
    this.locked = true;
    this.ridicZaznamService.upsertRidicZaznam(this.entry.ridicZaznam).subscribe(
      result => {
        this.entry.ridicZaznam = result;
        this.ref.markForCheck();
        this.entry.changed = false;
        if (emitSave) {
          this.saveEvent.emit(true);
        }
        this.locked = false;
      },
      error => {
        this.snackBar.open(
          'Databázový server neodpovídá. Zkuste to prosím později.',
          'Zkusit znovu'
        );
        this.snackBar._openedSnackBarRef.onAction().subscribe(() => {
          this.upsertRidicZaznam(emitSave);
        });
      }
    );
  }
}
