export const environment = {
  production: true,
  dotoUrl: 'https://ridic.dudr.cz/api/v1/',
  ridic: {
    zaznam: {
      zavezeno: {
        'Zavezeno, vše': 'Zavezeno, vše',
        'Zavezeno, částečně': 'Zavezeno, částečně (detaily uveďte v poznámce)',
        '-': '(nic k zavezení)',
        'Nezavezeno, nestihl': 'Nezavezeno, nestihl jsem',
        'Nezavezeno, nebylo vyrobeno': 'Nezavezeno, nebylo vyrobeno',
        'Nezavezeno, nikdo nezastižen': 'Nezavezeno, nikdo nezastižen',
        'Nezavezeno, neřešil': 'Nezavezeno, neřešil jsem'
      }
    },
    servis: {
      pás: [
        'CO',
        'V',
        'ST',
        'P',
        'O',
        'RO',
        'full servis',
        'popis na páse',
        'reklamace',
        'bouraný'
      ],
      kotouč: [
        'R',
        'O',
        'VJP',
        'KP',
        'full servis',
        'popis na kotouči',
        'reklamace',
        'bouraný',
        'určí mistr'
      ],
      katr: ['O', 'V', 'ST', 'full servis', 'popis na katru', 'bouraný'],
      jiné: []
    }
  }
};
