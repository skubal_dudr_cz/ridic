// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  dotoUrl: 'http://localhost:4202/api/',
  ridic: {
    zaznam: {
      zavezeno: {
        'Zavezeno, vše': 'Zavezeno, vše',
        'Zavezeno, částečně': 'Zavezeno, částečně (detaily uveďte v poznámce)',
        '-': '(nic k zavezení)',
        'Nezavezeno, nestihl': 'Nezavezeno, nestihl jsem',
        'Nezavezeno, nebylo vyrobeno': 'Nezavezeno, nebylo vyrobeno',
        'Nezavezeno, nikdo nezastižen': 'Nezavezeno, nikdo nezastižen',
        'Nezavezeno, neřešil': 'Nezavezeno, neřešil jsem'
      }
    },
    servis: {
      pás: [
        'CO',
        'V',
        'ST',
        'P',
        'O',
        'RO',
        'full servis',
        'popis na páse',
        'reklamace',
        'bouraný'
      ],
      kotouč: [
        'R',
        'O',
        'VJP',
        'KP',
        'full servis',
        'popis na kotouči',
        'reklamace',
        'bouraný',
        'určí mistr'
      ],
      katr: ['O', 'V', 'ST', 'full servis', 'popis na katru', 'bouraný'],
      jiné: []
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
